from machine import Pin
from neopixel import NeoPixel
from time import sleep

ROWS=16
COLS=16


matrix = NeoPixel(Pin(28, Pin.OUT),256)

def write_pixel(x, y,a,b,c):
    if y >= 0 and y < ROWS and x >=0 and x < COLS:
        # odd count rows 1, 3, 5 the wire goes from bottup
        if y % 2: 
            matrix[y*ROWS + x] = (a,b,c)             
        else: # even count rows, 0, 2, 4 the wire goes from the top down up
            matrix[(y+1)*ROWS - x-1] = (a,b,c)


x=7
y=8

a,b,c=50,0,0

step=1
state=0
numSteps=1
turnCounter=1
totalSteps = 256

def isPrime(value):
    if value==1:
        return False
    for i in range(2,value):
        if value%i==0:
            return False
    return True


for i in range(1,totalSteps+1):
    if isPrime(i):
        write_pixel(x,y,0,150,0)
        matrix.write()
        print(i)
    else:
        write_pixel(x,y,50,0,0)
        matrix.write()
    if state==0:
        x+=1

    if state==1:
        y-=1

    if state==2:
        x-=1

    if state==3:
        y+=1

    if step%numSteps==0:
        state=(state+1)%4
        turnCounter+=1
        if turnCounter%2==0:
            numSteps+=1
    step+=1
    #sleep(1)

